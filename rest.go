package main

import (
	"fmt"
	"github.com/julienschmidt/httprouter"
	log "gitlab.com/venchoware/go/log4go"
	"net/http"
	"time"
)

func initRoutes() *httprouter.Router {
	router := httprouter.New()

	router.POST("/v1/vendorcrawl/starts", vendorCrawlStarts)
	router.POST("/v1/vendorcrawl/finishes", vendorCrawlFinishes)
	router.POST("/v1/vendorcrawl/unnecessary", vendorCrawlUnnecessary)

	return router
}

func vendorCrawlStarts(
	_ http.ResponseWriter,
	r *http.Request,
	_ httprouter.Params,
) {
	vendorCrawlBase(r, "has started crawling", "start")
}

func vendorCrawlFinishes(
	w http.ResponseWriter,
	r *http.Request,
	p httprouter.Params,
) {
	vendorCrawlBase(r, "has finished crawling", "finish")
}

func vendorCrawlUnnecessary(
	w http.ResponseWriter,
	r *http.Request,
	p httprouter.Params,
) {
	vendorCrawlBase(r, "did not need to crawl", "unnecessary")
}

func vendorCrawlBase(r *http.Request, logMsg, crawlType string) {
	log.Debug("Body: " + fmt.Sprintf("%v", r.Body))
	err := r.ParseForm()
	if err != nil {
		log.Error("Problem parsing crawl data: %v", err)
	}
	log.Debug("Form: %v", r.PostForm)

	vendor := r.PostForm["vendor"][0]
	when, err := time.Parse(time.RFC3339, r.PostForm["when"][0])
	if err != nil {
		log.Error("Problem parsing crawl when: %v", err)
	}

	msg := fmt.Sprintf("Smusmabot %s %s at %v.", logMsg, vendor, when)
	log.Info(msg)

	err = mq.sendMessage(vendorCrawl{crawlType, vendor, when})
	if err != nil {
		log.Error("problem queueing crawl %s: %v", crawlType, err)
	}
}
