package main

import (
	"flag"
	"github.com/spf13/viper"
	log "gitlab.com/venchoware/go/log4go"
	"net/http"
)

var mq messageQueue

func main() {
	port := flag.String("port", "", "Port the service listens to")
	mq_service := flag.String("mq_service", "", "Message queue service")
	qname := flag.String("qname", "crawl_queue", "Queue name")
	loglevel := flag.String("loglevel", "", "Log level")
	flag.Parse()

	initEnvVars("smevents")

	if *loglevel != "" {
		viper.Set("loglevel", *loglevel)
	}
	lls := viper.GetString("loglevel")
	ll, err := log.Str2Level(lls)
	if err != nil {
		log.Fatal("Unknown log level: (%s)", lls)
	}
	log.NewDefaultLogger(ll)

	if *port != "" {
		viper.Set("port", *port)
	}
	if *port = viper.GetString("port"); *port == "" {
		log.Fatal("Port must be set!")
	}

	if *mq_service != "" {
		viper.Set("mq_service", *mq_service)
	}
	if *mq_service = viper.GetString("mq_service"); *mq_service == "" {
		log.Fatal("Message queue must be set!")
	}

	if *qname != "" {
		viper.Set("qname", *qname)
	}

	mq, err = initMQ(*mq_service)
	if err != nil {
		log.Fatal("Cannot connect to message queue (%s): %v",
			*mq_service, err)
	}
	defer mq.close()

	log.Info("Listening on port %s.", *port)
	log.Fatal(http.ListenAndServe(":"+viper.GetString("port"),
		initRoutes()))
}

func initEnvVars(prefix string) {
	viper.SetEnvPrefix(prefix)
	viper.BindEnv("port")
	viper.BindEnv("loglevel")
	viper.BindEnv("mq_service")
	viper.SetDefault("loglevel", "INFO")
}
