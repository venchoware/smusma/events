package main

import (
	"encoding/json"
	"fmt"
	retry "github.com/avast/retry-go"
	"github.com/spf13/viper"
	"github.com/streadway/amqp"
	"time"
)

type messageQueue struct {
	conn *amqp.Connection
	ch   *amqp.Channel
	q    amqp.Queue
}

type vendorCrawl struct {
	Type   string
	Vendor string
	When   time.Time
}

func initMQ(service string) (messageQueue, error) {
	var msgq messageQueue

	conn, err := amqp.Dial(service)
	if err != nil {
		return msgq, err
	}
	msgq.conn = conn

	ch, err := conn.Channel()
	if err != nil {
		return msgq, err
	}
	msgq.ch = ch

	q, err := ch.QueueDeclare(
		viper.GetString("qname"), // name
		false,                    // durable
		false,                    // delete when unused
		false,                    // exclusive
		false,                    // no-wait
		nil,                      // arguments
	)
	if err != nil {
		return msgq, err
	}
	msgq.q = q

	return msgq, nil
}

func (msgq messageQueue) close() {
	msgq.conn.Close()
	msgq.ch.Close()
}

func (msgq messageQueue) sendMessage(message vendorCrawl) error {
	body, err := json.Marshal(message)
	if err != nil {
		return fmt.Errorf("cannot encode message (%v): %v", message, err)
	}
	err = retry.Do(
		func() error {
			err := msgq.ch.Publish(
				"",          // exchange
				msgq.q.Name, // routing key
				false,       // mandatory
				false,       // immediate
				amqp.Publishing{
					ContentType: "application/json",
					Body:        body,
					Timestamp:   message.When,
				},
			)
			if err != nil {
				return err
			}
			return nil
		},
		retry.Attempts(5),
		retry.DelayType(retry.BackOffDelay),
		retry.LastErrorOnly(true),
		retry.RetryIf(func(err error) bool {
			return err != nil
		}),
	)
	return err
}
